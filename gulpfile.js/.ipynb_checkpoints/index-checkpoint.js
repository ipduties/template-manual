var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var log         = require('fancy-log');
var reload      = browserSync.reload;

const { src, dest, series }         = require('gulp');

const { git } = require('./git');


exports.git = series( git );


// Output commands on console
const exec = require('child_process').exec;

function make(done) {
	var command = 'make html';
	log('Befehl: ' + command);
	exec(command, (err, stdout, stderr) => log(stdout));
	done();
}


gulp.task('default', function () {

    // Serve files from the root of this project
    browserSync.init({
        port: 3010,
        open: false,
        server: {
            baseDir: "docs/build/html"
        }
    });
    gulp.watch("docs/source/**/*").on('all', gulp.series( make )); 
    //gulp.watch("docs/build/**/*.html").on('all', reload)
    gulp.watch( "docs/build/**/*.html", { events: ['all'], delay: 500  } , reload ).on('all', reload)

});


