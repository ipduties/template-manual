const { src, dest, symlink, series }  = require('gulp');
var   config                = require('../util/loadConfig').postprocess;

function postprocess(done) {
  var glob = "a";
  src(config.jekylltag).pipe(dest(config.basedir));
  // exec('touch $(config.basedir)$(config.jekylltag)');
  src(config.symlinktarget).pipe(symlink(config.basedir+config.baseurl+'/'));
  done();
};

exports.postprocess = series ( postprocess );
