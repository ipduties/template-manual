Spielweise zum Testen
=====================

#.. toctree::
#   :maxdepth: 2
#   
#   Wiese1
#   Wiese2

**Genau so sie es aus**

.. raw:: html 

    <h1 style="color:red">Hallo</h1>

Press :kbd:`ctrl` + :kbd:`s`

| These lines are
| broken exactly like in
| the source file.

This is a normal text paragraph. The next paragraph is a code sample::

   It is not processed in any way, except
   that the indentation is removed.

       It can span multiple lines.


This is a normal text paragraph again.

>>> 1 + 1
2

This is a paragraph that contains `a link`_.

.. _a link: https://domain.invalid/

This is a paragraph that contains `a link`_.
Hallo
.. _a link: https://domain.invalid/


Clicking on this internal hyperlink will take us to the target_
below.

Hier ist ein Test

.. _target:

The hyperlink target above points to this paragraph.

External hyperlink targets have an absolute or relative URI or email address in their link blocks. For example, take the following input:

See the Python_ home page for info.

`Write to me`_ with your questions.

.. _Python: http://www.python.org
.. _Write to me: jdoe@example.com

.. compound::

   The 'rm' command is very dangerous.  If you are logged
   in as root and enter ::

       cd /
       rm -rf *

   you will erase the entire contents of your file system.
   
.. code:: python

  def my_function():
      "just a test"
      print 8/2
      
